// Mevlüt Erdem Güven, Telegram@archi_dagac t.me/archi_dagac <merdem2002@mail.ru>, 2019
//
// https://gitlab.com/archi_dagac/xkb-kokturkce-dugmelik
//
//
// Köktürkçe düğmelik düzenidir. Düğme başına bir tamga gelecek biçimde dizilmiştir.
// Başka arkadaşların ince ve üst karakter olarak kalın tamgaları düğme başına iki tamga olarak dizdiği çalışmaları da bulabilirsiniz.
// Sorular için bana Telegram'dan ulaşabilirsiniz.
// 
// Öbeklerimiz ve akışlarımız (groups and channels): 
// Telegram dili: https://t.me/translation_kokturkce
// Oreo öncesi Android için yazı tipi dosyaları: https://t.me/kokturkceyazi (istek üzerine yazı tipi dosyası yapılır)
// Genel söyleşi: https://t.me/kokturkce
// Diğer: [@DroidTR](https://t.me/DroidTR) [@GNULinuxTR](https://t.me/GNULinuxTR) [@OpenWrtTurkiye](https://t.me/OpenWrtTurkiye) [@elektronikTR](https://t.me/ElektronikTR) 

//ÜK Düğmelik
// //𐰜:𐰓𐰇𐰍𐰢𐰀𐰠𐰃𐰚
//ÜK

//CTRL + C = CTRL + SHIFT + 𐰴
//CTRL + V = CTRL + SHIFT + 𐰖
//CTRL + A = CTRL + SHIFT + 𐰀
//CTRL + Z = CTRL + SHIFT + 𐰏

//
// Mevlüt Erdem Güven, [@archi_dagac](t.me/archi_dagac) <merdem2002@mail.ru>, 2019


// Default layout (ük keyboard) (based on Turkmen layout)
default partial
xkb_symbols "basic" {
    name[Group1]="KokTurkce";
 
    key <AE11>   { [   U10C45,   U2E2E   ] };
    key <AE12>   { [   U10C43,   minus,   underscore,   bar   ] };
    key <AD01>   { [   U10C1C,   U10C1C   ] };
    key <AD02>   { [   U10C38,   U10C38   ] };
    key <AD03>   { [   U10C31,   U10C31   ] };    
    key <AD04>   { [   U10C36,   U10C36   ] };
    key <AD05>   { [   U10C28,   U10C28   ] };
    key <AD06>   { [   U10C26,   U10C26   ] };
    key <AD07>   { [   U10C2A,   U10C2A   ] };
    key <AD08>   { [   U10C2D,   U10C2D   ] };
    key <AD09>   { [   U10C23,   U10C23   ] };
    key <AD10>   { [   U10C24,   U10C24   ] };
    key <AD11>   { [   U10C2F,   U10C2F   ] };
    key <AD12>   { [   U10C15,   backslash   ] };
    key <AC01>   { [   U10C0B,   U10C0B   ] };
    key <AC02>   { [   U10C09,   U10C09   ] };
    key <AC03>   { [   U10C0F,   z,   Z   ] };
    key <AC04>   { [   U10C0D,   U10C0D   ] };
    key <AC05>   { [   U10C13,   U10C13   ] };
    key <AC06>   { [   U10C11,   U10C11   ] };
    key <AC07>   { [   U10C20,   U10C20   ] };
    key <AC08>   { [   U10C1E,   U10C1E   ] };
    key <AC09>   { [   U10C1A,   U10C1A   ] };
    key <AC10>   { [   U10C34,   c,   C   ] };
    key <AC11>   { [   U10C41,   U10C41   ] };
    key <BKSL>   { [   U10C22,   U10C22   ] };
    key <TLDE>   { [   U10C05,   U10C21   ] };
    key <AB01>   { [   U10C07,   U10C07   ] };
    key <AB02>   { [   U10C03,   U10C03   ] };
    key <AB03>   { [   U10C00,   a,   A   ] };
    key <AB04>   { [   U10C3E,   U10C3E   ] };
    key <AB05>   { [   U10C3D,   U10C3D   ] };
    key <AB06>   { [   U10C18,   U10C18   ] };
    key <AB07>   { [   U10C16,   v,   V   ] };
    key <AB08>   { [   U10C3C,   U10C3C   ] };
    key <AB09>   { [   U10C3A,   U10C3A   ] };
    key <AB10>   { [   U10C32,   U10C32   ] };
    key <LSGT>   { [   U10C06,   less,   greater,   brokenbar   ] };
    key <SPCE>   { [   colon,   space   ] };

    include "level3(ralt_switch)"
};

