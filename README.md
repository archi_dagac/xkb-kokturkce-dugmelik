# X11-XKB Köktürkçe Düğmelik Düzeni
## (GNU/Linux vb. için) (Genellikle Bilgisayar için) X11-XKB Köktürkçe Düğmelik Düzeni

![Düğme Dizilimi](./dugmelik_duzeni.jpg "Düğme Dizilimi") <br>
![Düğme Dizilimi Simgelerle](./dugmelik_duzeni_simgelerle.png "Düğme Dizilimi Simgelerle")

_Kurulum aşağıdadır._

// Köktürkçe düğmelik düzenidir. Düğme başına bir tamga gelecek biçimde dizilmiştir. 
<br>
// Başka arkadaşların ince ve üst karakter olarak kalın tamgaları düğme başına iki tamga olarak dizdiği çalışmaları da bulabilirsiniz.
<br>
// Sorular için bana Telegram'dan ulaşabilirsiniz.
<br>

<br> 
// Öbeklerimiz ve akışlarımız (groups and channels): 
<br>
// Telegram dili: [@Translation_Kokturk](https://t.me/translation_kokturk)
<br>
// Oreo öncesi Android için yazı tipi dosyaları: [@KokturkceYazi](https://t.me/kokturkceyazi) (istek üzerine yazı tipi dosyası yapılır)
<br>
// Genel söyleşi: [@Kokturkce](https://t.me/kokturkce)
<br>
// Diğer: [@DroidTR](https://t.me/DroidTR) [@GNULinuxTR](https://t.me/GNULinuxTR) [@OpenWrtTurkiye](https://t.me/OpenWrtTurkiye) [@elektronikTR](https://t.me/ElektronikTR) 
<br>
// ÜK Düğmelik
<br>
// 𐰜:𐰓𐰇𐰍𐰢𐰀𐰠𐰃𐰚
<br>

<br>
İçerdiği kısayollar:
<br>
// CTRL + C = CTRL + SHIFT + 𐰴
<br>
// CTRL + V = CTRL + SHIFT + 𐰖
<br>
// CTRL + A = CTRL + SHIFT + 𐰀
<br>
// CTRL + Z = CTRL + SHIFT + 𐰏
<br>

<br>
Eklenmesini istediğiniz şeyler için iletişime geçebirisiniz.
<br>
<br>

**Kurulum:**

<br>

1.  `$ git clone https://gitlab.com/archi_dagac/xkb-kokturkce-dugmelik.git`
2.  `$ cd xkb-kokturkce-dugmelik`
3.  `# cp kt /usr/share/X11/xkb/symbols/`
4.  `/usr/share/X11/xkb/rules/evdev.xml` dosyasını aşağıdaki gösterildiği biçimde düzenle.
4. (English) Edit the `/usr/share/X11/xkb/rules/evdev.xml` file same as showed below. 
5. `# rm /var/lib/xkb/*.xkm`
6. ([For based] Debian, Ubuntu, Mint... [taban için]) `# dpkg-reconfigure xkb-data`
7. Oturumu kapat ve yeniden aç.
7. (English) Log out and log in again.

<br>
<br>

**Kurulum:**

<br>
"kt" dosyası `/usr/share/X11/xkb/symbols/` içine kopyalanacak. 
<br>
<br>
Şu dosya kök kullanıcı ile açılıp gerekli düzenlemeler yapılacak (nano, gedit veya leafpad gibi araçlarla) :

<br>

`/usr/share/X11/xkb/rules/evdev.xml`

<br>
<br>

`# nano /usr/share/X11/xkb/rules/evdev.xml`

<br>
<br>
Bu bölüm dosyanın içine Türkçenin hemen üzerine eklenecek:
<br>

    ...
    ...

    <layout>
      <configItem>
        <name>kt</name>

        <shortDescription>kt</shortDescription>
        <description>KökTürkçe</description>
        <languageList>
          <iso639Id>tur</iso639Id>
        </languageList>
      </configItem>
      <variantList>
      </variantList>
    </layout>

    ...
    ...

<br>
Yani tam olarak şunun yukarısına:

<br>

    ...
    ...

    <layout>
      <configItem>
        <name>tr</name>

        <shortDescription>tr</shortDescription>
        <description>Turkish</description>
        <languageList>
          <iso639Id>tur</iso639Id>
        </languageList>
      </configItem>
      <variantList>
        <variant>
          <configItem>
            <name>f</name>
            <description>Turkish (F)</description>
          </configItem>
        </variant>

    ...
    ...

<br>
Bittiğinde de şöyle görünmesi gerek:
<br>

    ...
    ...

    <layout>
      <configItem>
        <name>kt</name>

        <shortDescription>kt</shortDescription>
        <description>KökTürkçe</description>
        <languageList>
          <iso639Id>tur</iso639Id>
        </languageList>
      </configItem>
      <variantList>
      </variantList>
    </layout>
    <layout>
      <configItem>
        <name>tr</name>

        <shortDescription>tr</shortDescription>
        <description>Turkish</description>
        <languageList>
          <iso639Id>tur</iso639Id>
        </languageList>
      </configItem>
      <variantList>
        <variant>
          <configItem>
            <name>f</name>
            <description>Turkish (F)</description>
          </configItem>
        </variant>
        
    ...
    ...
<br>
<br>
Sırasıyla girmeniz gereken komutlar:
<br>

**1) Ortak:** 

<br>

`# rm /var/lib/xkb/*.xkm`

<br>
<br>

**2) Debian taban için (Ubuntu, Mint...):**

<br>

`# dpkg-reconfigure xkb-data`

<br>
<br>
<br>


// **Mevlüt Erdem Güven**, [@archi_dagac](https://t.me/archi_dagac) <merdem2002@mail.ru>, **2019**
